Source: libmmap-allocator
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: libdevel
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-exec
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/libmmap-allocator
Vcs-Git: https://salsa.debian.org/med-team/libmmap-allocator.git
Homepage: https://github.com/ekg/mmap_allocator/

Package: libmmap-allocator-dev
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: STL allocator that mmaps files
 When reading large files (>100MB) into memory, read() calls are usually
 not very space and time efficient, since the whole data is copiied at
 least once. Furthermore, when using STL containers (like a vector for
 example), data is copiied another time unless the location of the vector
 content as parameter to read() will be specified.
 .
 It would be nice to tell the vector a filename and have the vector mmap
 the file directly. This not only avoids the read() copiing (and the STL
 vector copiing) but also allows different processes that read the same
 file to see the same physical memory. Fortunately STL foresees an
 interface to do exactly this.
 .
 Libmmap-allocator helps to handle big files that contain unstructured
 data (like doubles or even text files), mmap_allocator is worth a try.
